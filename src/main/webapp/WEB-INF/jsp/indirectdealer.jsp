<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Dealer</title>
<jsp:include page="/WEB-INF/jsp/common/header.jsp"></jsp:include>
 <link href="/static/css/bootstrap.min.css" rel="stylesheet">
	
   <!-- Custom styles for this template -->
   <link href="/static/css/shop-homepage.css" rel="stylesheet">

	   <link href="/static/css/font-awesome.css" rel="stylesheet">
	
   <!-- Website Font style -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" />

   <!-- Google Fonts -->
   <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css' />
   <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css' />

   <!--bootstrap date-time picker-->
   <link href="static/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

   <!--data table-->
   <link href="static/css/jquery.dataTables.min.css" rel="stylesheet" />
   <link href="static/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="static/css/bootstrap.min.css" rel="stylesheet" />
	
   <link href="/static/css/main.css" rel="stylesheet" />
<style>
		.my-custom-scrollbar {
		position: relative;
		height: 80%;
		overflow: auto;
		width:320%;
		}
		.table-wrapper-scroll-y {
		display: block;
		}
		</style>
</head>
<body>
 </br>
 </br>
 </br>
 </br>
 </br>
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <h1 class="my-6"></h1>
        <div>
             <a href="/signup?person=indirectdealer" class="btn btn-dark btn-mg">Create Dealer</a>
        </div>
        <br/>
        <br/>
        <br/>
        
		<div class="table-wrapper-scroll-y my-custom-scrollbar">
		   <table class="table table-striped table-bordered mb-0 " width="100%" cellspacing="0">
					 <thead class="thead-dark">
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">First name</th>
					      <th scope="col">Last name</th>
					      <th scope="col">Phone</th>
					      <th scope="col">Email</th>
					      <th scope="col">Shipping Address</th>
					      <th scope="col">Shipping Address</th>
					      <th scope="col">Shipping City</th>
					      <th scope="col">Shipping State</th>
					      <th scope="col">Shipping Zip code</th>			      
					      <th scope="col">Billing Address</th>
					      <th scope="col">Billing Address</th>
					      <th scope="col">Billing City</th>
					      <th scope="col">Billing State</th>
					      <th scope="col">Billing Zip code</th>
					      <th scope="col">Edit</th>
						  <th scope="col">Enable/Disable</th>
					    </tr>
					  </thead>	
					   <tbody>
		  				<c:forEach var="indirectdealer" items="${indirectdealers}">  
					    <tr>
					      <th scope="row">1</th>
					      <td>${indirectdealer.firstName}</td>
					      <td>${indirectdealer.lastName}</td>
					      <td>${indirectdealer.phone}</td>
					      <td>${indirectdealer.email}</td>
					       <td>${indirectdealer.shippingstreet}</td>
					      <td>${indirectdealer.shippingapt}</td>
					      <td>${indirectdealer.shippingcity}</td>
					      <td>${indirectdealer.shippingstate}</td>
					       <td>${indirectdealer.shippingzipcode}</td>
					      <td>${indirectdealer.mailingstreet}</td>
					      <td>${indirectdealer.mailingapt}</td>
					      <td>${indirectdealer.mailingcity}</td>
					       <td>${indirectdealer.mailingstate}</td>
					      <td>${indirectdealer.mailingzipcode}</td>
						<td><a href="/edit/?id=${indirectdealer.id}" class="fa fa-edit"></a></td>	 
						<c:if test="${indirectdealer.active eq true}">
								<td><a href="/update?id=${indirectdealer.id}"><strong>Make Inactive</strong></a></td>	   
						</c:if>
						<c:if test="${indirectdealer.active ne true}">
								<td><a href="/update?id=${indirectdealer.id}"><strong>Make Active</strong></a></td>	   
						</c:if>
					</tr>			
				</c:forEach>
			 </tbody>
	       </table>
         </div>
      </div>
    </div>
  </div>
<div class="m 0"><jsp:include page="/WEB-INF/jsp/common/footer.jsp" /></div>
</body>
</html>