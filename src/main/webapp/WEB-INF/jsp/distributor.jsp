<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Distributor</title>
   <!-- Bootstrap core CSS -->
   <link href="/static/css/bootstrap.min.css" rel="stylesheet">
	
   <!-- Custom styles for this template -->
   <link href="/static/css/shop-homepage.css" rel="stylesheet">

	   <link href="/static/css/font-awesome.css" rel="stylesheet">
	
   <!-- Website Font style -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" />

   <!-- Google Fonts -->
   <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css' />
   <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css' />

   <!--bootstrap date-time picker-->
   <link href="static/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

   <!--data table-->
   <link href="static/css/jquery.dataTables.min.css" rel="stylesheet" />
   <link href="static/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="static/css/bootstrap.min.css" rel="stylesheet" />
	
   <link href="/static/css/main.css" rel="stylesheet" />
<style>
		.my-custom-scrollbar {
		position: relative;
		height: 80%;
		overflow: auto;
		width:320%;
		}
		.table-wrapper-scroll-y {
		display: block;
		}
		</style>
</head>
<body>
 </br>
 </br>
 </br>
 </br>
  <div class="container">
    <div class="row"><nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/welcome">Door Order System</a>
            &nbsp  &nbsp   &nbsp   &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp
            &nbsp  &nbsp   &nbsp   &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp
            &nbsp  &nbsp   &nbsp   &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp
            &nbsp  &nbsp   &nbsp   &nbsp  &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp
                         
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav">
                  <li class="nav-item active"><a class="nav-link" href="/welcome">Home</a></li>
          <li class="nav-item active"><a class="nav-link" href="/salesmanager">Sales Manager</a>          </li>
           <li class="nav-item active"><a class="nav-link" href="/salesperson">Sales Person</a></li>
          <li class="nav-item active"><a class="nav-link" href="/directdealer">Dealer</a>          </li>
          <li class="nav-item active"><a class="nav-link" href="/products" >Products</a>          </li>
          <li class="nav-item active"><a class="nav-link" href="/orders">Orders</a>          </li>
	   	  <li>
	          <div class="dropdown">
				  <button class="nav-link nav-item active text-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Menu
				  </button>
				   
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			  		<ul class="navbar-nav ml-auto">
			  		<li class="nav-item active">
			    	<a class="dropdown-item" href="/editpassword">Edit Password</a>
			    	<a class="dropdown-item" href="/logout">Logout</a>
			         
	     			</li>
	   				</ul>
	   			  </div>
		   	 </div>
	   	 </li>
	   	 </ul>
	</div>    
</div>
</nav>
      <div class="col-lg-4">
        <h1 class="my-6"></h1>
        <div>
         <a href="/signup?person=distributor" class="btn btn-dark btn-mg">Create Distributor</a>
        </div>
        <br/>
        <br/>
        <div class="table-wrapper-scroll-y my-custom-scrollbar">
		   <table class="table table-striped table-bordered mb-0" width="100%" cellspacing="0">
					 <thead class="thead-dark">
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">Company name</th>
					      <th scope="col">First name</th>
					      <th scope="col">Last name</th>
					      <th scope="col">Phone</th>
					      <th scope="col">Email</th>
					      <th scope="col">Shipping Address</th>
					      <th scope="col">Shipping Address</th>
					      <th scope="col">Shipping City</th>
					      <th scope="col">Shipping State</th>
					      <th scope="col">Shipping Zip code</th>
					      <th scope="col">Billing Address</th>
					      <th scope="col">Billing Address</th>
					      <th scope="col">Billing City</th>
					      <th scope="col">Billing State</th>
					      <th scope="col">Billing Zip code</th>
					      <th scope="col">Edit</th>
						  <th scope="col">Enable/Disable</th>
					    </tr>
					  </thead>	
					   <tbody>
		  				<c:forEach var="distributor" items="${distributors}" varStatus="i">  
					    <tr>
					      <td scope="row">${i.index+1}</td>
					      <td>${distributor.companyname}</td>
					      <td>${distributor.firstName}</td>
					      <td>${distributor.lastName}</td>
					      <td>${distributor.phone}</td>
					      <td>${distributor.email}</td>
					       <td>${distributor.shippingstreet}</td>
					      <td>${distributor.shippingapt}</td>
					      <td>${distributor.shippingcity}</td>
					      <td>${distributor.shippingstate}</td>
					       <td>${distributor.shippingzipcode}</td>
					      <td>${distributor.mailingstreet}</td>
					      <td>${distributor.mailingapt}</td>
					      <td>${distributor.mailingcity}</td>
					       <td>${distributor.mailingstate}</td>
					      <td>${distributor.mailingzipcode}</td>
						<td><a href="/edit/?id=${distributor.id}" class="fa fa-edit"></a></td>	 
						<c:if test="${distributor.active eq true}">
								<td><a href="/update?id=${distributor.id}"><strong>Make Inactive</strong></a></td>	   
						</c:if>
						<c:if test="${distributor.active ne true}">
								<td><a href="/update?id=${distributor.id}"><strong>Make Active</strong></a></td>	   
						</c:if>
						</tr>			
				</c:forEach>
				</tbody>
	      </table>
	      </br>
          </br>
          </br>
          </br>
        </div>
     </div>
    </div>
  </div>
<div class="m 0"><jsp:include page="/WEB-INF/jsp/common/footer.jsp" /></div>
</body>
</html>