<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Log In</title>
	 <jsp:include page="/WEB-INF/jsp/common/header.jsp"></jsp:include>    
</head>
<body>
<!-- Login Section -->
    <img class="img-responsive" src="/static/Images/banner.png" alt="banner"/>
    <div class="container">
        <div class="row ">
            <div class="main-center ">
           		<c:if test="${wrong}">
                           <span class="bg-danger pull-right">Please enter valid username</span>
                </c:if>
                <form class="login100-form validate-form" action="forgotpassword" method="post">
                    <span class="login100-form-title">
						Please enter your username
					</span>
                    <div class="wrap-input100 validate-input">
                        <label for="username" class="sr-only">Username</label>
                        <input type="text" roleId="username" class="form-control" placeholder="Username" name="username"
                               id="username"
                               required="required" autofocus="autofocus"/>
                    </div>
                 
                <div class="form-group ">
                    <input type="submit" class="btn btn-info btn-lg btn-block login-button" value="Forgot Password"/>
                </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
    </div>
</body>
</html>