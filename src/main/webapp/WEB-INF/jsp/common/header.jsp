<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Header</title>
   <!-- Bootstrap core CSS -->
   <link href="/static/css/bootstrap.min.css" rel="stylesheet">
	
   <!-- Custom styles for this template -->
   <link href="/static/css/shop-homepage.css" rel="stylesheet">

	   <link href="/static/css/font-awesome.css" rel="stylesheet">
	
   <!-- Website Font style -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" />

   <!-- Google Fonts -->
   <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css' />
   <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css' />

   <!--bootstrap date-time picker-->
   <link href="static/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

   <!--data table-->
   <link href="static/css/jquery.dataTables.min.css" rel="stylesheet" />
   <link href="static/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="static/css/bootstrap.min.css" rel="stylesheet" />
	
   <link href="/static/css/main.css" rel="stylesheet" />
	   
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/welcome">Door Order System</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/welcome">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li>
	          <div class="dropdown">
				  <button class="nav-link text-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Menu
				  </button>
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			  		<ul class="navbar-nav ml-auto">
			  		<li class="nav-item active">
			    	<a class="dropdown-item" href="/editpassword">Edit Password</a>
			    	<a class="dropdown-item" href="/logout">Logout</a>
	     			</li>
	   				</ul>
	   			  </div>
		   	 </div>
	   	 </li>
	   	 </ul>
	</div>    
</div>
</nav>
</body>
</html>