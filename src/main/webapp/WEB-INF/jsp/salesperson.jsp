<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  

<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Sales Person</title>
		   <!-- Bootstrap core CSS -->
   <link href="/static/css/bootstrap.min.css" rel="stylesheet">
	
   <!-- Custom styles for this template -->
   <link href="/static/css/shop-homepage.css" rel="stylesheet">

	   <link href="/static/css/font-awesome.css" rel="stylesheet">
	
   <!-- Website Font style -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" />

   <!-- Google Fonts -->
   <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css' />
   <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css' />

   <!--bootstrap date-time picker-->
   <link href="static/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

   <!--data table-->
   <link href="static/css/jquery.dataTables.min.css" rel="stylesheet" />
   <link href="static/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="static/css/bootstrap.min.css" rel="stylesheet" />
	
   <link href="/static/css/main.css" rel="stylesheet" />
		<style>
		.my-custom-scrollbar {
		position: relative;
		height: 80%;
		overflow: auto;
		width:320%;
		}
		.table-wrapper-scroll-y {
		display: block;
		}
		
		.fr{
		float:right !important;
		}
		</style>
	</head>
	<body>
	</br>
	</br>
	</br>
	</br>
	</br>
	</br>
		<div class="container">
		    <div class="row">
		    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/welcome">Door Order System</a> 
      &nbsp  &nbsp   &nbsp   &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp
       &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp
        &nbsp  &nbsp &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp &nbsp  &nbsp   &nbsp
      &nbsp  &nbsp   &nbsp       &nbsp  &nbsp   &nbsp
      
      
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav">
          <li ><a class="fr nav-link nav-item active" href="/salesmanager">Sales Manager</a></li>
          <li class="fr nav-item active"><a class="nav-link" href="/distributor">Distributor</a>          </li>
          <li class="fr nav-item active"><a class="nav-link" href="/directdealer">Dealer</a>          </li>
          <li class="fr nav-item active"><a class="nav-link" href="/products" >Products</a>          </li>
          <li class="fr nav-item active"><a class="nav-link" href="/orders">Orders</a>          </li>
          <li class="fr nav-item active"><a class="nav-link" href="/welcome">Home</a></li>
        
           

          <li>
	          <div class="dropdown">
				  <button class="nav-link nav-item active text-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Menu
				  </button>
				   
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			  		<ul class="navbar-nav ml-auto">
			  		<li class="nav-item active">
			    	<a class="dropdown-item" href="/editpassword">Edit Password</a>
			    	<a class="dropdown-item" href="/logout">Logout</a>
			         
	     			</li>
	   				</ul>
	   			  </div>
		   	 </div>
	   	 </li>

	   	 </ul>
	</div>    
</div>
</nav>
		      <div class="col-lg-4">
		        <h1 class="my-6"></h1>
					<div>
					<a href="/signup?person=salesperson" class="btn btn-dark btn-mg">Create Salesperson</a>
					</div>
					<br/>
					<br/>

					
			           <div class="table-wrapper-scroll-y my-custom-scrollbar">
			           <table class="table table-striped table-bordered mb-0" width="100%" cellspacing="0">
						 <thead class="thead-dark">
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">First name</th>
						      <th scope="col">Last name</th>
						      <th scope="col">Phone</th>
						      <th scope="col">Email</th>
						      <th scope="col">Edit</th>
						      <th scope="col">Enable/Disable</th>
						    </tr>
						  </thead>	
                		  <tbody>
			  				<c:forEach var="salesperson" items="${salespersons}">  
						    <tr>
						      <th scope="row">1</th>
						      <td>${salesperson.firstName}</td>
						      <td>${salesperson.lastName}</td>
						      <td>${salesperson.phone}</td>
						       <td>${salesperson.email}</td>
						       <td><a href="/edit/?id=${salesperson.id}" class="fa fa-edit"></a></td>	 
								<c:if test="${salesperson.active eq true}">
										<td><a href="/update/?id=${salesperson.id}"><strong>Make Inactive</strong></a></td>	   
								</c:if>
								<c:if test="${salesperson.active ne true}">
										<td><a href="/update/?id=${salesperson.id}"><strong>Make Active</strong></a></td>	   
								</c:if>
						    </tr>			
						</c:forEach>
			          </table>
					</div>
		        </div>
		     </div>
		    </div>
		<div class="m 0"><jsp:include page="/WEB-INF/jsp/common/footer.jsp" /></div>
	</body>
</html>