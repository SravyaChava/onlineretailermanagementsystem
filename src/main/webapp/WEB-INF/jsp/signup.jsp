<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
   
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <!-- Title Page-->
    <title>Sign Up</title>

    <!-- Icons font CSS-->
    <link href="static/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="static/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="static/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="static/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="static/css/main.css" rel="stylesheet" media="all">
    <style type="text/css">
    .label1 {
	  display: block;
	  padding-left: 15px;
	  text-indent: -15px;
	  padding-top:4px;
	  }
	.input1 {
	  width: 13px;
	  height: 13px;
	  padding-top: 0;
	  margin:0;
	  
	  vertical-align: bottom;
	  position: relative;
	  top: -1px;
	  *overflow: hidden;
	  }
	  
	  ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: currentColor;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover {
  background-color: #111;
}

.card{
margin-top:-110px;
}
</style>
</head>
<body>
<ul>
  <li><a href="/welcome">Door Order System</a></li>
  <li style="float:right"><a class="active" href="#home">Home</a></li>
  <li style="float:right"><a href="#contact">Contact</a></li>
  <li style="float:right "><a href="#about">About</a></li>
</ul>
    <div class="page-wrapper bg-gra-01 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4 ">
                <div class="card-body">
                    <h2 class="title">Registration Form</h2>
                      <form:form class="form-horizontal" method="post" action="signup" modelAttribute="user">
                        <input type="hidden" name="person" value="${person}"/>
                        <c:if test="${(person == 'distributor')}">
	                    <div class="row row-space">
	                    	<div class="col-2">
	                    		<div class="input-group">
	                    			<label class="label">Company name</label>
                                	<form:input class="input--style-4" value="${user.companyname}" id="companyname" path="companyname"  placeholder="Enter your Company Name" required="required"/>  	
	                            </div>
	                       </div>
	                    </div>
	                    </c:if>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">First name</label>
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <form:input class="input--style-4" type="text" value="${user.firstName}" id="firstName" path="firstName" placeholder="Enter your first name" required="required"/>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Last name</label>
                            		<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            		<form:input class="input--style-4" type="text" value="${user.lastName}" id="lastName" path="lastName"  placeholder="Enter your last name" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Phone</label>
                                    <span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
                                    <div class="input-group-icon">
                                     <form:input class="input--style-4" value="${user.phone}" id="phone" name="phone" path="phone"  placeholder="xxx-xxx-xxxx" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <c:if test="${emailExists}">
                                         <span class="bg-danger pull-right">Email already exists</span>
                                   </c:if>
                            		<form:input class="input--style-4" value="${user.email}" id="email" path="email"  placeholder="Enter your Email" required="required"/>
                                </div>
                            </div>
                        </div>                        
                        <c:if test="${(person == 'distributor') || (person == 'directdealer') ||(person=='indirectdealer')}">
	                    <div class="row row-space">
	                    	<div class="col-2">
	                    		<div class="input-group">
	                    			<label class="label">Your Billing Address</label>
                                	<form:input class="input--style-4" value="${user.mailingstreet}" id="mailingstreet" path="mailingstreet"  placeholder="Enter your Mailing Street" required="required"/>  	
	                            </div>
	                       </div>
	                       <div class="col-2">
	                       		<div class="input-group">
	                       			<label class="label">Your Billing Address</label>
	                       			<form:input class="input--style-4" value="${user.mailingapt}" id="mailingapt" path="mailingapt"  placeholder="Enter your Mailing Apt" required="required"/>
	                            </div>
	                       </div>
	                    </div>
	                    <div class="row row-space">
	                    	<div class="col-2">
	                    		<div class="input-group">
	                    			<label class="label">Your Billing City</label>
	                       		<form:input class="input--style-4" value="${user.mailingcity}" id="mailingcity" path="mailingcity"  placeholder="Enter your Mailing City" required="required"/>
	                            </div>
	                       </div>
	                       <div class="col-2">
	                       		<div class="input-group">
	                       			<label class="label">Your Billing State</label>
	                                <form:input class="input--style-4" value="${user.mailingstate}" id="mailingstate" path="mailingstate"  placeholder="Enter your Mailing State" required="required"/>
	                            </div>
	                       </div>
	                    </div>
	                    <div class="row row-space">
	                    	<div class="col-2">
	                    		<div class="input-group">
	                    			<label class="label">Your Billing Zip code</label>
	                                <form:input class="input--style-4" value="${user.mailingstreet}" id="mailingzipcode" path="mailingzipcode"  placeholder="Enter your Mailing Zipcode" required="required"/>	                            </div>
	                       </div>
	                    </div>
	                    <div class="checkbox">
                            <label class="label1">
                                <input class="input1" id="isshippingaddress" type="checkbox" onclick="sameaddress()"/>Check if Shipping address is same as mailing address?
                            </label>
                        </div>
                        <br/>                        
	                 	 <div class="row row-space">
	                    	<div class="col-2">
	                    		<div class="input-group">
	                    			<label class="label">Your Shipping Address</label>
	                        	<form:input class="input--style-4" value="${user.shippingstreet}" id="shippingstreet" path="shippingstreet"  placeholder="Enter your shipping Street" required="required"/>
	                            </div>
	                       </div>
	                       <div class="col-2">
	                       		<div class="input-group">
	                       			<label class="label">Your Shipping Address</label>
	                       		<form:input class="input--style-4" value="${user.shippingapt}" id="shippingapt" path="shippingapt"  placeholder="Enter your shipping Apt" required="required"/>
	                            </div>
	                       </div>
	                    </div>
	                    <div class="row row-space">
	                    	<div class="col-2">
	                    		<div class="input-group">
	                    			<label class="label">Your Shipping City</label>
	                        	<form:input class="input--style-4" value="${user.shippingcity}" id="shippingcity" path="shippingcity"  placeholder="Enter your shipping City" required="required"/>
	                            </div>
	                       </div>
	                       <div class="col-2">
	                       		<div class="input-group">
	                       			<label class="label">Your Shipping State</label>
	                            <form:input class="input--style-4" value="${user.shippingstate}" id="shippingstate" path="shippingstate"  placeholder="Enter your shipping State" required="required"/>
	                            </div>
	                       </div>
	                    </div>
	                    <div class="row row-space">
	                    	<div class="col-2">
	                    		<div class="input-group">
	                    			<label class="label">Your Shipping Zip code</label>
	                            <form:input class="input--style-4" value="${user.shippingzipcode}" id="shippingzipcode" path="shippingzipcode"  placeholder="Enter your shipping Zipcode" required="required"/>	                            
	                            </div>
	                       </div>
	                    </div>
	                 	</c:if>
                   		<div class="row row-space">
                			<c:if test="${usernameExists}">
                			<span class="pull-right">Username already exists</span>
                			</c:if>
                    		<div class="col-2">
                        		<div class="input-group">
		                    		<label class="label">Username</label>
                            		<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            		<form:input class="input--style-4" value="${user.username}" id="username" path="username"  placeholder="Enter your Username" required="required"/>
                        		</div>
                    		</div>
                    		<div class="col-2">
                        		<div class="input-group col-1">
		                    		<label class="label">Password</label>
		                    		<form:errors path="password" cssStyle="color: #ff0000"></form:errors>
                            		<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            		<form:password class="input--style-4" path="password" id="password" placeholder="Enter your Password" required="required"/>
                            		<br/>
                     				<div class="input-group">
                            		<label class="label1"><input id="showPassword"  type="checkbox" onClick="showme()" class="input1"/>Show Password</label>
                            		</div>
                        		</div>
                    		</div>
	                     </div>
						 <div class="p-t-15">
                    		<button style={margin-left:100px} type="submit" class="btn btn--radius-2 btn--blue">Create!</button>
                    		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                    		 <a  class="btn btn--radius-2 btn--blue" onclick="window.history.go(-1); return false">Cancel</a>
                		</div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
	<script type="text/javascript">
	function showme(){
		if(document.getElementById("showPassword").checked){
			document.getElementById('password').type="text";
		}
		else{
			document.getElementById('password').type="password";
			}
		}
     </script>
     <script type="text/javascript">
	function sameaddress(){
		if(document.getElementById("isshippingaddress").checked){
			document.getElementById('shippingstreet').value=document.getElementById('mailingstreet').value;
			document.getElementById('shippingapt').value=document.getElementById('mailingapt').value;
			document.getElementById('shippingcity').value=document.getElementById('mailingcity').value;
			document.getElementById('shippingstate').value=document.getElementById('mailingstate').value;
			document.getElementById('shippingzipcode').value=document.getElementById('mailingzipcode').value;
		}
		else{
			document.getElementById('shippingstreet').value="";
			document.getElementById('shippingapt').value="";
			document.getElementById('shippingcity').value="";
			document.getElementById('shippingstate').value="";
			document.getElementById('shippingzipcode').value="";
			}
		}
      </script>
    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>
    <!-- Main JS-->
    <script src="js/global.js"></script>
</body>
</html>
