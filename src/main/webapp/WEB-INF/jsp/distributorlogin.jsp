<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Distributor</title>
  <!-- Bootstrap core CSS -->
  <link href="/static/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="/static/css/shop-homepage.css" rel="stylesheet">
  <jsp:include page="/WEB-INF/jsp/common/header.jsp" />
  <style>
.fonsize{
	font-size: 20px;
	font-color: 'black'
}
</style>
</head>

<body style="text-align:center">
  <div class="container" style="margin-top:6%; margin-left:-1%">
    <div class="row">
      <div class="col-lg-4">
        <h1 class="my-6"></h1>
         <div>
          <a href="/indirectdealer" class="list-group-item fonsize"><strong>Dealer</strong></a>
        </div>
        <div class="list-group">
          <a href="/orders" class="list-group-item fonsize"><strong>Orders</strong></a>
        </div>
     </div>
    </div>
  </div>
<div><jsp:include page="/WEB-INF/jsp/common/footer.jsp" /></div>
</body>
</html>
