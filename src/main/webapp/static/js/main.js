/**
 * Created by z00382545 on 10/20/16.
 */

(function ($) {
    $.toggleShowPassword = function (options) {
        var settings = $.extend({
            field: "#password",
            control: "#toggle_show_password",
        }, options);

        var control = $(settings.control);
        var field = $(settings.field)

        control.bind('click', function () {
            if (control.is(':checked')) {
                field.attr('type', 'text');
            } else {
                field.attr('type', 'password');
            }
        })
    };

    $.transferDisplay = function () {
        $("#transferFrom").change(function() {
            if ($("#transferFrom").val() == 'Primary') {
                $('#transferTo').val('Savings');
            } else if ($("#transferFrom").val() == 'Savings') {
                $('#transferTo').val('Primary');
            }
        });

        $("#transferTo").change(function() {
            if ($("#transferTo").val() == 'Primary') {
                $('#transferFrom').val('Savings');
            } else if ($("#transferTo").val() == 'Savings') {
                $('#transferFrom').val('Primary');
            }
        });
    };



}(jQuery));

$(document).ready(function() {
    var confirm = function() {
        bootbox.confirm({
            title: "Appointment Confirmation",
            message: "Do you really want to schedule this appointment?",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $('#appointmentForm').submit();
                } else {
                    console.log("Scheduling cancelled.");
                }
            }
        });
    };

    $.toggleShowPassword({
        field: '#password',
        control: "#showPassword"
    });

    $.transferDisplay();

    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:mm",
        autoclose: true,
        todayBtn: true,
        startDate: "2013-02-14 10:00",
        minuteStep: 10
    });

    $('#submitAppointment').click(function () {
        confirm();
    });

});













(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);








jQuery(document).ready(function( $ ) {

	  // Back to top button
	  $(window).scroll(function() {
	    if ($(this).scrollTop() > 100) {
	      $('.back-to-top').fadeIn('slow');
	    } else {
	      $('.back-to-top').fadeOut('slow');
	    }
	  });
	  $('.back-to-top').click(function(){
	    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
	    return false;
	  });

	  // Initiate the wowjs animation library
	  new WOW().init();

	  // Initiate superfish on nav menu
	  $('.nav-menu').superfish({
	    animation: {
	      opacity: 'show'
	    },
	    speed: 400
	  });

	  // Mobile Navigation
	  if ($('#nav-menu-container').length) {
	    var $mobile_nav = $('#nav-menu-container').clone().prop({
	      id: 'mobile-nav'
	    });
	    $mobile_nav.find('> ul').attr({
	      'class': '',
	      'id': ''
	    });
	    $('body').append($mobile_nav);
	    $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
	    $('body').append('<div id="mobile-body-overly"></div>');
	    $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

	    $(document).on('click', '.menu-has-children i', function(e) {
	      $(this).next().toggleClass('menu-item-active');
	      $(this).nextAll('ul').eq(0).slideToggle();
	      $(this).toggleClass("fa-chevron-up fa-chevron-down");
	    });

	    $(document).on('click', '#mobile-nav-toggle', function(e) {
	      $('body').toggleClass('mobile-nav-active');
	      $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
	      $('#mobile-body-overly').toggle();
	    });

	    $(document).click(function(e) {
	      var container = $("#mobile-nav, #mobile-nav-toggle");
	      if (!container.is(e.target) && container.has(e.target).length === 0) {
	        if ($('body').hasClass('mobile-nav-active')) {
	          $('body').removeClass('mobile-nav-active');
	          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
	          $('#mobile-body-overly').fadeOut();
	        }
	      }
	    });
	  } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
	    $("#mobile-nav, #mobile-nav-toggle").hide();
	  }

	  // Smooth scroll for the menu and links with .scrollto classes
	  $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
	    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      if (target.length) {
	        var top_space = 0;

	        if ($('#header').length) {
	          top_space = $('#header').outerHeight();

	          if( ! $('#header').hasClass('header-fixed') ) {
	            top_space = top_space - 20;
	          }
	        }

	        $('html, body').animate({
	          scrollTop: target.offset().top - top_space
	        }, 1500, 'easeInOutExpo');

	        if ($(this).parents('.nav-menu').length) {
	          $('.nav-menu .menu-active').removeClass('menu-active');
	          $(this).closest('li').addClass('menu-active');
	        }

	        if ($('body').hasClass('mobile-nav-active')) {
	          $('body').removeClass('mobile-nav-active');
	          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
	          $('#mobile-body-overly').fadeOut();
	        }
	        return false;
	      }
	    }
	  });

	  // Header scroll class
	  $(window).scroll(function() {
	    if ($(this).scrollTop() > 100) {
	      $('#header').addClass('header-scrolled');
	    } else {
	      $('#header').removeClass('header-scrolled');
	    }
	  });

	  // Intro carousel
	  var introCarousel = $(".carousel");
	  var introCarouselIndicators = $(".carousel-indicators");
	  introCarousel.find(".carousel-inner").children(".carousel-item").each(function(index) {
	    (index === 0) ?
	    introCarouselIndicators.append("<li data-target='#introCarousel' data-slide-to='" + index + "' class='active'></li>") :
	    introCarouselIndicators.append("<li data-target='#introCarousel' data-slide-to='" + index + "'></li>");
	  });

	  $(".carousel").swipe({
	    swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
	      if (direction == 'left') $(this).carousel('next');
	      if (direction == 'right') $(this).carousel('prev');
	    },
	    allowPageScroll:"vertical"
	  });

	  // Skills section
	  $('#skills').waypoint(function() {
	    $('.progress .progress-bar').each(function() {
	      $(this).css("width", $(this).attr("aria-valuenow") + '%');
	    });
	  }, { offset: '80%'} );

	  // jQuery counterUp (used in Facts section)
	  $('[data-toggle="counter-up"]').counterUp({
	    delay: 10,
	    time: 1000
	  });

	  // Porfolio isotope and filter
	  var portfolioIsotope = $('.portfolio-container').isotope({
	    itemSelector: '.portfolio-item',
	    layoutMode: 'fitRows'
	  });

	  $('#portfolio-flters li').on( 'click', function() {
	    $("#portfolio-flters li").removeClass('filter-active');
	    $(this).addClass('filter-active');

	    portfolioIsotope.isotope({ filter: $(this).data('filter') });
	  });

	  // Clients carousel (uses the Owl Carousel library)
	  $(".clients-carousel").owlCarousel({
	    autoplay: true,
	    dots: true,
	    loop: true,
	    responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 6 }
	    }
	  });

	  // Testimonials carousel (uses the Owl Carousel library)
	  $(".testimonials-carousel").owlCarousel({
	    autoplay: true,
	    dots: true,
	    loop: true,
	    items: 1
	  });

	});




(function ($) {
	"use strict";
	$('.column100').on('mouseover',function(){
		var table1 = $(this).parent().parent().parent();
		var table2 = $(this).parent().parent();
		var verTable = $(table1).data('vertable')+"";
		var column = $(this).data('column') + ""; 

		$(table2).find("."+column).addClass('hov-column-'+ verTable);
		$(table1).find(".row100.head ."+column).addClass('hov-column-head-'+ verTable);
	});

	$('.column100').on('mouseout',function(){
		var table1 = $(this).parent().parent().parent();
		var table2 = $(this).parent().parent();
		var verTable = $(table1).data('vertable')+"";
		var column = $(this).data('column') + ""; 

		$(table2).find("."+column).removeClass('hov-column-'+ verTable);
		$(table1).find(".row100.head ."+column).removeClass('hov-column-head-'+ verTable);
	});
    

})(jQuery);
