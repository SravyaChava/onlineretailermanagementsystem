package com.example.OnlineRetailerManagement.Controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.OnlineRetailerManagement.Model.Doorassembly;
import com.example.OnlineRetailerManagement.Model.Doorframe;
import com.example.OnlineRetailerManagement.Model.Doorhanding;
import com.example.OnlineRetailerManagement.Model.Doorheight;
import com.example.OnlineRetailerManagement.Model.Doorpaneltype;
import com.example.OnlineRetailerManagement.Model.Doorunitfinish;
import com.example.OnlineRetailerManagement.Model.Doorwidth;
import com.example.OnlineRetailerManagement.Model.Dummyitems;
import com.example.OnlineRetailerManagement.Model.Manufacturer;
import com.example.OnlineRetailerManagement.Model.UserRole;
import com.example.OnlineRetailerManagement.Model.Userclass;
import com.example.OnlineRetailerManagement.Repositories.DoorAssemblyRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorFrameRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorFrameThicknessRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorGlassCoatingRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorGlassThicknessRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorGlassTypeRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorHandingRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorHandleColorRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorHandleRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorHeightRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorLockRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorPanelTypeRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorTypeRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorUnitFinishRepository;
import com.example.OnlineRetailerManagement.Repositories.DoorWidthRepository;
import com.example.OnlineRetailerManagement.Repositories.PredrillRepository;
import com.example.OnlineRetailerManagement.Repositories.RoleDao;
import com.example.OnlineRetailerManagement.Repositories.SalesManagerRepository;
import com.example.OnlineRetailerManagement.Repositories.SalesPersonRepository;
import com.example.OnlineRetailerManagement.Repositories.UserDao;
import com.example.OnlineRetailerManagement.Repositories.UserRepository;
import com.example.OnlineRetailerManagement.service.GeneratePasswordService;
import com.example.OnlineRetailerManagement.service.SmtpMailSender;
import com.example.OnlineRetailerManagement.service.UserService;

import net.bytebuddy.implementation.bind.annotation.BindingPriority;

@Controller
public class DoorController {
	@Autowired
	private DoorTypeRepository doortyperepository;
	@Autowired
	private DoorFrameRepository doorframerepository;
	@Autowired
	private DoorWidthRepository doorwidthrepository;
	@Autowired
	private DoorHeightRepository doorheightrepository;
	@Autowired
	private DoorPanelTypeRepository doorpaneltyperepository;
	@Autowired
	private DoorHandingRepository doorhandingrepository ;
	@Autowired
	private DoorAssemblyRepository doorassemblyrepository;
	@Autowired
	private DoorUnitFinishRepository doorunitfinishrepository;
	
	@Autowired
	private SmtpMailSender smtpMailSender;
	
	/*@Autowired
	private DoorGlassTypeRepository doorglasstyperepository;
	@Autowired
	private DoorGlassCoatingRepository doorglasscoatingrepository;
	@Autowired
	private DoorGlassThicknessRepository doorglassthicknessrepository;
	@Autowired
	private DoorHandleRepository doorhandlerepository;
	@Autowired
	private DoorHandleColorRepository doorhandlecolorrepository;
	@Autowired
	private DoorLockRepository doorlockrepository;
	@Autowired
	private DoorFrameThicknessRepository doorframethicknessrepository;
	@Autowired
	private PredrillRepository predrillrepository;*/
	@Autowired
	private Dummyitems dummyitems;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private UserDao userDao;	
	@Autowired
	private Manufacturer manufacturer;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;	
	@Autowired
	private SalesManagerRepository salesmanagerrepository;
	@Autowired
	private SalesPersonRepository salespersonrepository;
	@Autowired
	private UserRepository userrepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private GeneratePasswordService generatePasswordService;
	
	static Long userId = 0l;
	
	@RequestMapping("/")
	public String home(Model model) {
		System.out.print("index");
		return "redirect:/login";
	}
	
	@RequestMapping("/login")
	public String login(){
	return "login";
	}
	
	@RequestMapping(value="/signup", method = RequestMethod.GET)
	public String signup(Model model, @RequestParam(value="person",required=false) String person){
		Userclass userclass=new Userclass();
		model.addAttribute("person",person);
		model.addAttribute("user",userclass);
		return "signup";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(Principal principal,@Valid @ModelAttribute("user") Userclass userclass,BindingResult bindingResult, Model model,@RequestParam(value="person", required = false) String person) {
		if(bindingResult.hasErrors()) {
			return "signup";
		}
		System.out.print(userclass.toString());
		if(userService.checkUsernameExists(userclass.getUsername())) {
				model.addAttribute("usernameExists",true);
				model.addAttribute("user",userclass);
				model.addAttribute("person","");
				System.out.print("in this method" + "userexists");
				return "signup";
		}else {
			userclass.setPassword(passwordEncoder.encode(userclass.getPassword()));
			userclass.setActive(true);
			Set<UserRole> userroles = new HashSet<>();
			System.out.println("hi");
			long id=0;
			try {
			id=userDao.findmaxuserid();
			System.out.print(id);
			}catch (Exception e) {
				// TODO: handle exception
				System.out.print("exception occured");
				e.printStackTrace();
			}
			if(userDao.findmaxuserid() == null) userId=1l;
			else userId=id+1;			
			userclass.setId(userId);
			if(userId ==1)
				userroles.add(new UserRole(userId,userclass,roleDao.findByName("ROLE_ADMIN")));
			else			
				userroles.add(new UserRole(userId,userclass,roleDao.findByName("ROLE_USER")));
			userclass.getUserroles().addAll(userroles);
			userDao.save(userclass);
		}
		
		userclass=userDao.findByUsername(principal.getName());
		System.out.println(userclass.getPerson());
		String p = userclass.getPerson();
		if(p.equals("salesmanager")||p.equals("salesperson")||p.equals("distributor")||p.equals("directdealer")||p.equals("indirectdealer"))
		return "redirect:/"+userclass.getPerson()+"login";
		else
			return "manufacturer";
	}
	
	@RequestMapping("/edit")
	public String edit(@ModelAttribute("user") Userclass userclass,@RequestParam(value="id", required=true) Long id, HttpServletRequest request) {
		Userclass usern = userrepository.findbyId(id);
		System.out.print(usern.getPerson());
		request.setAttribute("person", usern.getPerson());
		request.setAttribute("edituser", usern);
		return "editsignup";
	}
	
	@RequestMapping("/update")
	public String update(@RequestParam(value="id", required=true) Long id, HttpServletRequest request, Principal principal) {
		Userclass usern = userrepository.findbyId(id);
		System.out.println("id is"+ usern.getId());
		if(usern.isActive()) {
			usern.setActive(false);
		}else {
			usern.setActive(true);
		}
		userDao.save(usern);
		String p = usern.getPerson();
		return "redirect:/"+p;
	}
	
	
	@Transactional
	@RequestMapping(value="/edit/editsave")
	public String editsave(Model model,@ModelAttribute("edituser") Userclass userclass,@RequestParam(value="id", required=false) Long id,HttpServletRequest request) {
		Userclass usern = userrepository.findbyId(id);

		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println(userclass.getFirstName());
		System.out.println(userclass.getId());
		System.out.println(userclass.getUsername());
		System.out.println(userclass.getPassword());
//		Set<UserRole> userroles = new HashSet<>();
//		if(userId ==1)
//			userroles.add(new UserRole(userId,userclass,roleDao.findByName("ROLE_ADMIN")));
//		else			
//			userroles.add(new UserRole(userId,userclass,roleDao.findByName("ROLE_USER")));
//		
//		userclass.getUserroles().addAll(userroles);
		String epassword = userclass.getPassword();
		userclass.setPassword(passwordEncoder.encode(epassword));
		userDao.save(userclass);
//		userclass.getFirstName(;
//		userclass.getLastName(),userclass.getPhone(),userclass.getEmail(),user.getId());
		return "redirect:/"+usern.getPerson();
	}
		
	
		
	@RequestMapping("/welcome")
	public String welcome(HttpServletRequest request,Model model,Principal principal) {
		return "welcome";
	}
	
	@RequestMapping("/welcomesubmit")
	public String welcomesubmit(HttpServletRequest request,Model model,Principal principal) {
		System.out.println(principal.getName());
		String person = userDao.findPersonName(principal.getName());
		System.out.println("person in welcome submit"+person);
		if(person.length()==0) return "manufacturer";
		else
		return person+"login";
	}
	
	@RequestMapping(path="/forgotpassword",method = RequestMethod.GET)
	public String forgotPassword() {
		return "forgot";
	}
	@RequestMapping(path = "/forgotpassword",method = RequestMethod.POST)
	public String forgotpassword(HttpServletRequest request) {
		try {
			String username=request.getParameter("username");
			Userclass userclass=userDao.findByUsername(username);
			if(userclass!=null) {
			String pass=generatePasswordService.generateRandomSpecialCharacters(8);
			userclass.setPassword(passwordEncoder.encode(pass));
			userDao.save(userclass);
			System.out.println(username);
			System.out.println(pass);
			smtpMailSender.send(username, "Subject" , "<h1>Please use this password to login: "+ pass + "</h1>");
			System.out.print("Mail sent");
			}else {
				request.setAttribute("wrong", true);
				return "forgot";
			}
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:/login";
	}
	
	@RequestMapping("/salesmanager")
	public String salesmanager(HttpServletRequest request) {
		List<Userclass> list=salesmanagerrepository.findByperson();
		request.setAttribute("salesmanagers",list);
		return "salesmanager";
	}
	
	@RequestMapping("/salesperson")
	public String salesperson(HttpServletRequest request) {
		request.setAttribute("salespersons",salespersonrepository.findByperson());
		return "salesperson";
	}
	
	@RequestMapping("/distributor")
	public String distributor(HttpServletRequest request) {
		List<Userclass> list=userrepository.findbydistributor();
		request.setAttribute("distributors",list);
		return "distributor";
	}
	
	@RequestMapping("/directdealer")
	public String directdealer(HttpServletRequest request) {
		List<Userclass> list=userrepository.findbydirectdealers();
		request.setAttribute("directdealers",list);
		return "directdealer";
	}
	@RequestMapping("/indirectdealer")
	public String indirectdealer(HttpServletRequest request) {
		List<Userclass> list=userrepository.findbyindirectdealers();
		request.setAttribute("indirectdealers",list);
		return "indirectdealer";
	}
	
	@RequestMapping("/manufacturerproducts")
	public String manufacturerproducts(HttpServletRequest request,Model model,Principal principal) {
		return "manufacturerproducts";
	}
	
	@RequestMapping("/distributorlogin")
	public String distributor(HttpServletRequest request,Model model,Principal principal) {
		return "distributorlogin";
	}
	
	@RequestMapping("/products")
	public String home(HttpServletRequest request,Model model,Principal principal) {
		System.out.println(principal.getName());
		request.setAttribute("doortypes",doortyperepository.findAll());
		return "doortype";
	}
	
	@RequestMapping("/editpassword")
	public String editPassword() {
		return "editpassword";
	}
	@RequestMapping("/changepassword")
	public String changePassword(Principal principal, HttpServletRequest request) {
		try {
		Userclass userclass=userDao.findByUsername(principal.getName());
		System.out.println("password: "+ request.getParameter("password"));
		userclass.setPassword(passwordEncoder.encode(request.getParameter("password")));
		userDao.save(userclass);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/welcomesubmit";
	}
	@GetMapping(value="/doorframe/{id}")
	public String screen(HttpServletRequest request,@PathVariable long id ) {
		dummyitems.setDoortypeid(id);
		List<Doorframe> doorframes = doorframerepository.findBydoorframename(id);
		System.out.print(doorframes);
		request.setAttribute("doorframes",doorframes);
			return "doorframe";
	}

	@GetMapping("/doorwidth/{doorframeid}")
	public String doorwidth(HttpServletRequest request,@PathVariable long doorframeid){
		long doortypepropid= dummyitems.getDoortypeid();
		dummyitems.setDoorframeid(doorframeid);
		List<Doorwidth> doorwidths = doorwidthrepository.findBydoorwidth(doortypepropid,doorframeid);		
		request.setAttribute("doorwidths",doorwidths);
		System.out.print(doorwidths);
			return "doorwidth";
	}
	
	@GetMapping("/doorheight/{doorwidthid}")
	public String doorheight(HttpServletRequest request,@PathVariable long doorwidthid){
		long doortypepropid= dummyitems.getDoortypeid();
		long doorframeid = dummyitems.getDoorframeid();
		dummyitems.setDoorwidthid(doorwidthid);
		List<Doorheight> doorheights = doorheightrepository.findBydoorheight(doortypepropid,doorframeid,doorwidthid);
		request.setAttribute("doorheights",doorheights);
		System.out.print(doorheights);
			return "doorheight";
	}
	
	@GetMapping("/doorpanel/{doorheightid}")
	public String doorpanel(HttpServletRequest request,@PathVariable long doorheightid) {
		dummyitems.setDoorheightid(doorheightid);
		long doortypepropid= dummyitems.getDoortypeid();
		long doorframeid = dummyitems.getDoorframeid();
		long doorwidthid = dummyitems.getDoorwidthid();
		/*String doortypename= doortyperepository.findBydoortype(dummyitems.getDoortypeid());
		String doorframename = doorframerepository.findBydoorframe(dummyitems.getDoorframeid());
		Long doorwidth = doorwidthrepository.finddoorwidth(dummyitems.getDoorwidthid());
		Long doorheight = doorheightrepository.findBydoorheight(dummyitems.getDoorheightid());
		String orderitemstring = doortypename + doorframename+doorwidth+doorheight;
		System.out.print(doortypename+doorframename+doorwidth+doorheight);
		return "order";*/
		List<Doorpaneltype> doorpanels = doorpaneltyperepository.findBydoorpanel(doortypepropid,doorframeid,doorwidthid,doorheightid);
		System.out.println("height id are"+doortypepropid+" "+doorframeid+" "+doorwidthid+
				" "+doorheightid+" ");
		request.setAttribute("doorpanels",doorpanels);
		return "doorpanel";
	}
	
	@GetMapping("/doorhanding/{doorpanelid}")
	public String doorhanding(HttpServletRequest request,@PathVariable long doorpanelid) {
		dummyitems.setDoorpanelid(doorpanelid);
		long doortypepropid= dummyitems.getDoortypeid();
		long doorframeid = dummyitems.getDoorframeid();
		long doorwidthid = dummyitems.getDoorwidthid();
		long doorheightid = dummyitems.getDoorheightid();
		List<Doorhanding> doorhandings = doorhandingrepository.findBydoorhanding(doortypepropid,doorframeid,doorwidthid,doorheightid, doorpanelid);
		System.out.println("panel id are"+doortypepropid+" "+doorframeid+" "+doorwidthid+
				" "+doorheightid+" "+doorpanelid);
		request.setAttribute("doorhandings",doorhandings);
		return "doorhanding";
	}
	
	@GetMapping("/doorassembly/{doorhandingid}")
	public String doorassembly(HttpServletRequest request,@PathVariable long doorhandingid) {
		dummyitems.setDoorhandingid(doorhandingid);
		long doortypepropid= dummyitems.getDoortypeid();
		long doorframeid = dummyitems.getDoorframeid();
		long doorwidthid = dummyitems.getDoorwidthid();
		long doorheightid = dummyitems.getDoorheightid();
		long doorpanelid = dummyitems.getDoorpanelid();
		System.out.println("handing id are"+doortypepropid+" "+doorframeid+" "+doorwidthid+
				" "+doorheightid+" "+doorpanelid+" "+doorhandingid);
		List<Doorassembly> doorassemblies = doorassemblyrepository.findBydoorassembly(doortypepropid,doorframeid,doorwidthid,doorheightid,doorpanelid,doorhandingid);
		System.out.println("doorassemblies are"+doorassemblies);
		request.setAttribute("doorassemblies",doorassemblies);
		return "doorassembly";
	}
	
	@GetMapping("/doorunitfinish/{doorassemblyid}")
	public String order(HttpServletRequest request,@PathVariable long doorassemblyid) {
		dummyitems.setDoorassemblyid(doorassemblyid);
		long doortypepropid= dummyitems.getDoortypeid();
		long doorframeid = dummyitems.getDoorframeid();
		long doorwidthid = dummyitems.getDoorwidthid();
		long doorheightid = dummyitems.getDoorheightid();
		long doorpanelid = dummyitems.getDoorpanelid();
		long doorhandingid = dummyitems.getDoorhandingid();
		List<Doorunitfinish> doorunitfinishes = doorunitfinishrepository.findBydoorunitfinish(doortypepropid,doorframeid,doorwidthid,doorheightid, doorpanelid, doorhandingid,doorassemblyid);
		request.setAttribute("doorunitfinishes",doorunitfinishes);
		return "doorunitfinish";
	}
		
	@GetMapping("/order/{doorunitfinishid}")
	public String order(HttpServletRequest request) {
		String doortypename= doortyperepository.findBydoortype(dummyitems.getDoortypeid());
		String doorframename = doorframerepository.findBydoorframe(dummyitems.getDoorframeid());
		String doorwidth = doorwidthrepository.finddoorwidth(dummyitems.getDoorwidthid());
		String doorheight = doorheightrepository.findBydoorheight(dummyitems.getDoorheightid());
		String doorpanel = doorpaneltyperepository.findBydoorpanel(dummyitems.getDoorpanelid());
		String doorhanding = doorhandingrepository.findBydoorhanding(dummyitems.getDoorhandingid());
		String doorassembly = doorassemblyrepository.findBydoorassembly(dummyitems.getDoorassemblyid());
		String doorunitfinish = doorunitfinishrepository.findBydoorunitfinish(dummyitems.getDoorunitfinishid());
		String orderitemstring = doortypename + doorframename+doorwidth+doorheight+doorpanel+doorhanding+doorassembly+doorunitfinish;
		System.out.print(orderitemstring);
		List<String> orderitems = new ArrayList<String>();
		orderitems.add(doortypename);
		orderitems.add(doorframename);
		orderitems.add(doorwidth);
		orderitems.add(doorheight);
		orderitems.add(doorpanel);
		orderitems.add(doorhanding);
		orderitems.add(doorassembly);
		orderitems.add(doorunitfinish);
		request.setAttribute("orderitems", orderitems);
		return "order";
	}
}
