package com.example.OnlineRetailerManagement.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.OnlineRetailerManagement.Model.Userclass;


@Repository
public interface SalesManagerRepository extends JpaRepository<Userclass, Long> {
	
	@Query("select u from Userclass u where u.person = 'salesmanager'")
	List<Userclass> findByperson(); 
	
}
