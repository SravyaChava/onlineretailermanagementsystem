package com.example.OnlineRetailerManagement.Repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.OnlineRetailerManagement.Model.Userclass;

@Repository
public interface UserRepository extends JpaRepository<Userclass, Long>{
	@Query("select u from Userclass u where u.id = ?1")
	Userclass findbyId(Long id);

	@Query("select u from Userclass u where u.person = 'distributor'")
	List<Userclass> findbydistributor();
	
	@Query("select u from Userclass u where u.person = 'directdealer'")
	List<Userclass> findbydirectdealers();
	
	@Query("select u from Userclass u where u.person = 'indirectdealer'")
	List<Userclass> findbyindirectdealers();
}