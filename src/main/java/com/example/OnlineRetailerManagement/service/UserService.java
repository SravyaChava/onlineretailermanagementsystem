package com.example.OnlineRetailerManagement.service;

import javax.transaction.Transactional;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.OnlineRetailerManagement.Model.Userclass;
import com.example.OnlineRetailerManagement.Repositories.UserDao;

@Transactional
@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;

	public boolean checkUsernameExists(String username) {
		// TODO Auto-generated method stub
		Userclass userclass=userDao.findByUsername(username);
		if(userclass !=null) {
			return true;
		}
		return false;
	}
	

}
