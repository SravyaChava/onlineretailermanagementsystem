package com.example.OnlineRetailerManagement.service;

import org.springframework.stereotype.Component;

import net.bytebuddy.utility.RandomString;

@Component
public class GeneratePasswordService {
	public String generateRandomSpecialCharacters(int length) {
		return RandomString.make(length);
	}
	
}
