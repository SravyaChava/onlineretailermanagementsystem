package com.example.OnlineRetailerManagement.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name="Product")
public class Product {
	@Id
	private long productid;
	private long doortypeid;
	private long doorframeid;
	private long doorwidthid;
	private long doorheightid;
	private long doorpanelid;
	private long doorhandingid;
	private long doorassemblyid;
	private long doorunitfinishid;
	/*private long doorglasstypeid;
	private long doorglassdesignid;
	private long doorglasscoatingid;
	private long doorglassthicknessid;
	private long doorhandleid;
	private long doorhandlecolorid;
	private long doorlockid;
	private long predrillid;
	private long doorframethicknessid;*/
	Product(){}
	public Product(long productid, long doortypeid, long doorframeid, long doorwidthid, long doorheightid,
			long doorpanelid, long doorhandingid, long doorassemblyid, long doorunitfinishid) {
		this.productid = productid;
		this.doortypeid = doortypeid;
		this.doorframeid = doorframeid;
		this.doorwidthid = doorwidthid;
		this.doorheightid = doorheightid;
		this.doorpanelid = doorpanelid;
		this.doorhandingid = doorhandingid;
		this.doorassemblyid = doorassemblyid;
		this.doorunitfinishid = doorunitfinishid;
		/*this.doorglasstypeid = doorglasstypeid;
		this.doorglassdesignid = doorglassdesignid;
		this.doorglasscoatingid = doorglasscoatingid;
		this.doorglassthicknessid = doorglassthicknessid;
		this.doorhandleid = doorhandleid;
		this.doorhandlecolorid = doorhandlecolorid;
		this.doorlockid = doorlockid;
		this.predrillid = predrillid;
		this.doorframethicknessid = doorframethicknessid;*/
	}
	public long getProductid() {
		return productid;
	}
	public void setProductid(long productid) {
		this.productid = productid;
	}
	public long getDoortypeid() {
		return doortypeid;
	}
	public void setDoortypeid(long doortypeid) {
		this.doortypeid = doortypeid;
	}
	public long getDoorframeid() {
		return doorframeid;
	}
	public void setDoorframeid(long doorframeid) {
		this.doorframeid = doorframeid;
	}
	public long getDoorwidthid() {
		return doorwidthid;
	}
	public void setDoorwidthid(long doorwidthid) {
		this.doorwidthid = doorwidthid;
	}
	public long getDoorheightid() {
		return doorheightid;
	}
	public void setDoorheightid(long doorheightid) {
		this.doorheightid = doorheightid;
	}
	public long getDoorpanelid() {
		return doorpanelid;
	}
	public void setDoorpanelid(long doorpanelid) {
		this.doorpanelid = doorpanelid;
	}
	public long getDoorhandingid() {
		return doorhandingid;
	}
	public void setDoorhandingid(long doorhandingid) {
		this.doorhandingid = doorhandingid;
	}
	public long getDoorassemblyid() {
		return doorassemblyid;
	}
	public void setDoorassemblyid(long doorassemblyid) {
		this.doorassemblyid = doorassemblyid;
	}
	public long getDoorunitfinishid() {
		return doorunitfinishid;
	}
	public void setDoorunitfinishid(long doorunitfinishid) {
		this.doorunitfinishid = doorunitfinishid;
	}
	@Override
	public String toString() {
		return "Product [productid=" + productid + ", doortypeid=" + doortypeid + ", doorframeid=" + doorframeid
				+ ", doorwidthid=" + doorwidthid + ", doorheightid=" + doorheightid + ", doorpanelid=" + doorpanelid
				+ ", doorhandingid=" + doorhandingid + ", doorassemblyid=" + doorassemblyid + ", doorunitfinishid="
				+ doorunitfinishid + "]";
	}

	

}
